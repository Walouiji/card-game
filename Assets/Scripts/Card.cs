using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Card : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public string Element;
    public Text l_Element;

    public GameObject Model;

    void Start()
    {
        l_Element.text = Element;
    }

    public void OnPointerEnter(PointerEventData pointerEventData)
	{
        Vector3 pos = transform.position;
        pos.y = 275;
        transform.position = pos;
        transform.localScale = Vector2.one * 1.3f;
    }
    public void OnPointerExit(PointerEventData pointerEventData)
    {
        Vector4 self = gameObject.transform.position;
        gameObject.transform.position = new Vector2(self.x, self.y=25);
        gameObject.transform.localScale = new Vector2(1, 1);
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            GameObject child = GameObject.FindGameObjectWithTag("Player");
            if(child != null)
            {
                Destroy(child);
            }
        }
    }
}
