using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Vuforia;

public class Game : MonoBehaviour
{
    private Card[] AvailableCards;

    List<Card> EnnemyCards = new List<Card>();
    List<Card> cards = new List<Card>();
    Button[] CardButtons = new Button[3];
    public VirtualButtonBehaviour[] Hand = new VirtualButtonBehaviour[3];
    public VirtualButtonBehaviour StartBattle = new VirtualButtonBehaviour();


    private Card PlayerCard;
    private Card EnnemyCard;
    private bool HasEnnemy = false;
    private bool HasPlayer = false;

    public Canvas canvas;
    private bool isStarted = false;

    private GameObject PlayerInstance;
    private GameObject EnnemyInstance;


    public Pawner EnnemyPawner;

 
    // Start is called before the first frame update
    void Start()
    {
        StartBattle.RegisterOnButtonPressed(Battle);
        AvailableCards = Resources.LoadAll<Card>("Cards");
        for (int i = 0; i < 3; ++i)
        {
            //Assigner une case du tableau par une des 3 cartes de mani�re al�atoire
            cards.Add(Instantiate(AvailableCards[i]));
            Card localCard = cards[i];
            CardButtons[i] = cards[i].GetComponent<Button>();
            CardButtons[i].onClick.AddListener(delegate { OnCardClicked(localCard); });
            CardButtons[i].onClick.AddListener(delegate { OnButtonPressed(localCard); });
            Hand[i].RegisterOnButtonPressed(delegate { OnButtonPressed(localCard); });

            //M�me chose qu'au dessus mais pour la main ennemie
            EnnemyCards.Add(Instantiate(AvailableCards[i]));
        }
    }

    void Update()
    {
        if (HasPlayer)
        {
            if (!HasEnnemy)
            {
                EnnemyCard = EnnemyCards[1];
                Pawner EnnemyPawner = GameObject.Find("EnnemyPawner").GetComponent<Pawner>();
                GameObject EnnemyChild = GameObject.FindGameObjectWithTag("Player");
                EnnemyInstance = Instantiate(EnnemyCard.Model, EnnemyPawner.transform);
                HasEnnemy = true;
            }
        }
        if (isStarted && HasEnnemy)
        {
            if (PlayerCard != null && EnnemyCard != null)
            {
                CheckWinner(PlayerCard, EnnemyCard);
                PlayerCard = null;
                EnnemyCard = null;
                isStarted = false;
            }
        }
    }

    void OnCardClicked(Card carte)
    {
        Pawner pawn = GameObject.Find("PlayerPawner").GetComponent<Pawner>();
        if (!HasPlayer)
        {
            PlayerInstance = Instantiate(carte.Model, pawn.transform);
            //Destroy(carte.gameObject);
            HasPlayer = true;
            carte.transform.SetParent(pawn.transform);
            PlayerCard = carte;
            //cards.Remove(carte);
            Debug.Log(PlayerCard);
            carte.gameObject.SetActive(false);
        }
    }

    void CheckWinner(Card Player, Card Ennemy)
    {
        Animator anim_player = PlayerCard.Model.GetComponent<Animator>();

        if (PlayerCard.Element == "water" && EnnemyCard.Element == "fire" || PlayerCard.Element == "fire" && EnnemyCard.Element == "grass" || PlayerCard.Element == "grass" && EnnemyCard.Element == "water")
        {
            anim_player.SetBool("IsOnBattle", true);
            anim_player.SetBool("IsWin", true);
            Debug.Log("WIN");

        }
        else if (PlayerCard.Element == "fire" && EnnemyCard.Element == "water" || PlayerCard.Element == "grass" && EnnemyCard.Element == "fire" || PlayerCard.Element == "water" && EnnemyCard.Element == "grass")
        {
            anim_player.SetBool("IsOnBattle", true);
            anim_player.SetBool("IsWin", false);
            Debug.Log("Loose");
        }
        else
        {
            anim_player.SetBool("IsOnBattle", true);
            anim_player.SetBool("IsWin", true);
            Debug.Log("Draw");
        }
        Destroy(PlayerInstance);
        Destroy(EnnemyInstance);
        HasEnnemy = false;
        HasPlayer = false;
    }
    void OnButtonPressed(Card carte)
    {
        Debug.Log(carte.Element);
        Pawner pawn = GameObject.Find("PlayerPawner").GetComponent<Pawner>();
        if (!HasPlayer)
        {
            PlayerInstance = Instantiate(carte.Model, pawn.transform);
            //Destroy(carte.gameObject);
            HasPlayer = true;
            carte.transform.SetParent(pawn.transform);
            PlayerCard = carte;
            //cards.Remove(carte);
            Debug.Log(PlayerCard);
            carte.gameObject.SetActive(false);
        }
    }

    void Battle(VirtualButtonBehaviour vb)
    {
        Debug.Log("battle started");
        isStarted = true;
    }

}
