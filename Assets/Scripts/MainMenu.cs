using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public Button ui_startButton;
    public Button ui_quitButton;
    public 
    // Start is called before the first frame update
    void Start()
    {
        ui_startButton.onClick.AddListener(OnClick);
        ui_quitButton.onClick.AddListener(OnQuit);
    }

    void OnClick()
	{
        SceneManager.LoadScene("Scene");
	}
    void OnQuit()
    {
        Application.Quit();
    }
}
